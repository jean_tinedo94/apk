package ejemplo.codictados.app.retrofit;

public class ListViewRec {
    private int imagen;
    private String nombre;
    private String visitas;

    public ListViewRec(int imagen, String nombre, String visitas) {
        this.imagen = imagen;
        this.nombre = nombre;
        this.visitas = visitas;
    }

    public String getNombre() {
        return nombre;
    }

    public String getVisitas() {
        return visitas;
    }

    public int getImagen() {
        return imagen;
    }
}