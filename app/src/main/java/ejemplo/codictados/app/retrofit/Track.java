
package ejemplo.codictados.app.retrofit;

import android.support.annotation.NonNull;

import java.util.Iterator;

public class Track implements Iterable
{
    private Image[] image;

    private String mbid;

    private String listeners;

    private String streamable;

    private Artist artist;

    private String playcount;

    private String name;

    private String url;

    public Image[] getImage ()
    {
        return image;
    }

    public void setImage (Image[] image)
    {
        this.image = image;
    }

    public String getMbid ()
    {
        return mbid;
    }

    public void setMbid (String mbid)
    {
        this.mbid = mbid;
    }

    public String getListeners ()
    {
        return listeners;
    }

    public void setListeners (String listeners)
    {
        this.listeners = listeners;
    }

    public String getStreamable ()
    {
        return streamable;
    }

    public void setStreamable (String streamable)
    {
        this.streamable = streamable;
    }

    public Artist getArtist ()
    {
        return artist;
    }

    public void setArtist (Artist artist)
    {
        this.artist = artist;
    }

    public String getPlaycount ()
    {
        return playcount;
    }

    public void setPlaycount (String playcount)
    {
        this.playcount = playcount;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [image = "+image+", mbid = "+mbid+", listeners = "+listeners+", streamable = "+streamable+", artist = "+artist+", playcount = "+playcount+", name = "+name+", url = "+url+"]";
    }

    @NonNull
    @Override
    public Iterator iterator() {
        return null;
    }
}
