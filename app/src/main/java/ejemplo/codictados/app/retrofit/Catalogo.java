
package ejemplo.codictados.app.retrofit;

import java.util.HashMap;
import java.util.Map;

public class Catalogo
{
    private Toptracks toptracks;

    public Toptracks getToptracks ()
    {
        return toptracks;
    }

    public void setToptracks (Toptracks toptracks)
    {
        this.toptracks = toptracks;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [toptracks = "+toptracks+"]";
    }
}

