package ejemplo.codictados.app.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Restclient {
    public static final String BASE_URL = "http://ws.audioscrobbler.com/2.0/";
    @GET("?method=artist.getTopTracks&artist=shakira&api_key=829751643419a7128b7ada50de590067&format=json")
    Call<Catalogo> getData();
}
