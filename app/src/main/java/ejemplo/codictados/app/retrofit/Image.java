package ejemplo.codictados.app.retrofit;

public class Image
{
    private String text;

    private String size;

    public String gettext ()
{
    return text;
}

    public void settext (String text)
{
    this.text = text;
}

    public String getSize ()
    {
        return size;
    }

    public void setSize (String size)
    {
        this.size = size;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [#text = "+text+", size = "+size+"]";
    }
}
