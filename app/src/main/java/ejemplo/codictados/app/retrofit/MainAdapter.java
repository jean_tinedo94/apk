package ejemplo.codictados.app.retrofit;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ejemplo.codictados.app.R;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.AnimeViewHolder> {
    private List<ListViewRec> items;

    public static class AnimeViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public ImageView imagen;
        public TextView nombre;
        public TextView visitas;

        public AnimeViewHolder(View v) {
            super(v);
            imagen = (ImageView) v.findViewById(R.id.imagen);
            nombre = (TextView) v.findViewById(R.id.nombre);
            visitas = (TextView) v.findViewById(R.id.visitas);
        }
    }

    public MainAdapter(List<ListViewRec> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        try {
            if (items.size()!=0) {
                return items.size();
            }
            else {
                return Integer.parseInt(null);
            }
        } catch (Exception e) {
        }
//        return Integer.parseInt(null);
        return 0;
    }

    @Override
    public AnimeViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.vistaelemento, viewGroup, false);
        return new AnimeViewHolder(v);
    }


    @Override
    public void onBindViewHolder(AnimeViewHolder viewHolder, int i) {
        viewHolder.imagen.setImageResource(items.get(i).getImagen());

        Log.e("prueba","cambio: "+items.get(i).getNombre());
        Log.e("prueba","cambio: "+items.get(i).getVisitas());
        Log.e("prueba","cambio: "+viewHolder.nombre);
        viewHolder.nombre.setText(items.get(i).getNombre());
        viewHolder.visitas.setText("Visitas:"+String.valueOf(items.get(i).getVisitas()));
    }
}