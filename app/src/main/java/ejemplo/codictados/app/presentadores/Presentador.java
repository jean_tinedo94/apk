package ejemplo.codictados.app.presentadores;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ejemplo.codictados.app.casos_uso.ClassOperations;
import ejemplo.codictados.app.casos_uso.Operacion;
import ejemplo.codictados.app.contratos.Contrato;

public class Presentador implements Contrato.Presentador {

    public static final int SUMA = 0;
    public static final int RESTA = 1;
    public static final int MULTIPLICACION = 2;
    public static final int DIVISION = 3;
    public static final int NINGUNA = 4;
    List resultado;
    Contrato.Vista vista;
    private int tipoOperacion = NINGUNA;

    // El presentador recibe su vista para devolverle cosas.
    @Override
    public void setVista(Contrato.Vista vista) {
        this.vista = vista;
    }
    @Override
    public void setTipoOperacion(View view) {
        switch (view.getId()){
            default: tipoOperacion = NINGUNA;
        }
        vista.mostrarTipoOperacion(obtenerCaracterOperacion());
    }

    public char obtenerCaracterOperacion(){
        char caracterOperacion;
        switch(tipoOperacion){
            case SUMA: caracterOperacion = '+'; break;
            case RESTA: caracterOperacion = '-'; break;
            case MULTIPLICACION: caracterOperacion = 'x'; break;
            case DIVISION: caracterOperacion = '/'; break;
            default: caracterOperacion = ' ';
        }
        return caracterOperacion;
    }


    // Cuando la vista pide al presentador el resultado de la operación, el presentador ejecutará
    // el caso de uso pertinente.
    @Override
    public void calcularResultado(){
        final List[] resultado = new List[1];
        final Operacion operacion;
        operacion = new ClassOperations();
        // Ejecuta el caso de uso
        operacion.calcular();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        Log.i("tag", "This'll run 300 milliseconds later");
                        resultado[0] = operacion.getResultado();
                        vista.mostrarLista(resultado[0]);
                    }
                },
                5000);

    }
}
