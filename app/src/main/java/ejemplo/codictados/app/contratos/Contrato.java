package ejemplo.codictados.app.contratos;

import android.view.View;

import java.util.List;

public interface Contrato {

    interface Vista {

        void getOperandos(String[] operandos);
        void mostrarError(String error);
        void mostrarResultado(int text);
        void mostrarLista(List e);
        void mostrarTipoOperacion(char caracter);


    }

    interface Presentador {

        void setVista(Vista vista);
        void setTipoOperacion(View view);
        void calcularResultado();
    }
}
