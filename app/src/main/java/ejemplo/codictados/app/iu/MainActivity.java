package ejemplo.codictados.app.iu;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import dagger.ObjectGraph;
import ejemplo.codictados.app.R;
import ejemplo.codictados.app.contratos.Contrato;
import ejemplo.codictados.app.dagger.MainModule;
import ejemplo.codictados.app.retrofit.MainAdapter;

public class MainActivity extends AppCompatActivity implements Contrato.Vista {


    @Inject Contrato.Presentador presentador;

    private FloatingActionButton fab;
    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main_activity_contenedor);

        // Inyecta las clases con Dagger. Esto solo lo tenemos aquí por simplicidad.
        ObjectGraph objectGraph = ObjectGraph.create(new MainModule());
        objectGraph.inject(this);

        // Le dice al presenter cuál es su vista
        presentador.setVista(this);

        loadView();
    }


    public void loadView(){
        recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);
        fab = (FloatingActionButton) findViewById(R.id.realizar_operacion);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogShow();
                presentador.calcularResultado();

                }
        });
}

    private View.OnClickListener listenerSeleccionarOperacion = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            presentador.setTipoOperacion(view);
        }
    };

    @Override
    public void getOperandos(String[] operandos) {
    }

    // El presentador llamará a este método para devolver el resultado de la operación solicitada.
    @Override
    public void mostrarResultado(int resultado){
    }

    @Override
    public void mostrarLista(List resultado){

        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);
// Crear un nuevo adaptador
        adapter = new MainAdapter(resultado);
        recycler.setAdapter(adapter);
    }
    // El presentador llamará a este método para devolver el carácter de la operación marcada.
    @Override
    public void mostrarTipoOperacion(char caracter){
       // tvOperacion.setText(String.valueOf(caracter));
    }

    @Override
    public void mostrarError(String error) {
        Snackbar.make(fab, error, Snackbar.LENGTH_SHORT).show();
    }
    private void DialogShow(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.espera).setIcon(R.drawable.ic_user);
        builder.setMessage("Cargando Elementos");
        final AlertDialog mDialog = builder.create();
// para hacerlo modal
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(null != mDialog && mDialog.isShowing()) mDialog.dismiss();
            }
        }, 5500);
    }
}
