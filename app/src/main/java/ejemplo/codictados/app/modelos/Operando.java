package ejemplo.codictados.app.modelos;

public class Operando {

    private int valor;

    public Operando(int valor) {
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }
}
