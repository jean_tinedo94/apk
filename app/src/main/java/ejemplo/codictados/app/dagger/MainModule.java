package ejemplo.codictados.app.dagger;

import dagger.Module;
import dagger.Provides;
import ejemplo.codictados.app.contratos.Contrato;
import ejemplo.codictados.app.iu.MainActivity;
import ejemplo.codictados.app.presentadores.Presentador;

@Module(injects = {MainActivity.class})
public class MainModule {

    @Provides
    public Contrato.Presentador provideMainActivityPresenter(){
        return new Presentador();
    }
}
