package ejemplo.codictados.app.casos_uso;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ejemplo.codictados.app.R;
import ejemplo.codictados.app.modelos.Operando;
import ejemplo.codictados.app.retrofit.ListViewRec;
import ejemplo.codictados.app.retrofit.Catalogo;
import ejemplo.codictados.app.retrofit.Restclient;
import ejemplo.codictados.app.retrofit.Track;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ClassOperations implements Operacion {


    private Operando operando1;
    private Operando operando2;
    private List resultado;
    public ClassOperations() {
        this.operando1 = operando1;
        this.operando2 = operando2;
    }

    @Override
    public void calcular() {
     ConsumoRetroFit();
     }
    public void ConsumoRetroFit(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Retrofit retrofit  = new Retrofit.Builder()
                .baseUrl(Restclient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        Restclient service = retrofit.create(Restclient.class);
        Call<Catalogo> requesCatalogo = service.getData();
        requesCatalogo.enqueue(new Callback<Catalogo>() {
                                   @Override
                                   public void onResponse(Call<Catalogo> call, Response<Catalogo> response) {
                                       if (!response.isSuccessful()) {
                                           Log.e("TAG", "Error" + response.errorBody()+ response.isSuccessful()+ "call"+ call);
                                       } else {
                                               Catalogo user1 = response.body();
                                               Log.e("ERROR", "prueba: " + user1.getToptracks().getTrack());
                                               Log.e("ERROR", "track1: " + user1.getToptracks().getTrack().toString());
                                               final List items = new ArrayList();
                                               for (Track track : user1.getToptracks().getTrack()) {
                                               Log.e("prueba","no se que poner"+ track.getName()+ "otra prueba"+ track.getListeners()+ "tercera prueba"+ track.getArtist().getName());
                                               items.add(new ListViewRec(R.drawable.ic_calcular,track.getName(),track.getArtist().getName()));
                                               }
                                               resultado=items;
                                           }
                                   }
                                   @Override
                                   public void onFailure(Call<Catalogo> call, Throwable t) {
                                    Log.e("TAG","call"+ call+ t);
                                   }
        });
     }

    @Override
    public List getResultado() {
        return resultado;
    }
}
