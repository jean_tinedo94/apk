package ejemplo.codictados.app.casos_uso;

import java.util.List;

public interface Operacion {

    void calcular();
    List getResultado();
}
